package edu.byui.andersonj.hibernate;


import javax.persistence.*;

@Entity
@Table(name = "ff7character")
public class Characters {

    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idFF7Character")
    private int idFF7Character;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private int age;

    @Column(name = "weapon")
    private String weapon;


    public int getIdFF7Characters() {
        return idFF7Character;
    }

    public void setIdFF7Character(int idFF7Character) {
        this.idFF7Character = idFF7Character;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getage() {
        return age;
    }

    public void setage(int age) {
        this.age = age;
    }

    public String getweapon() {
        return weapon;
    }

    public void setweapon(String weapon) {
        this.weapon = weapon;
    }

    public String toString() {
        if (age == 0) {
            return Integer.toString(idFF7Character) + " " + name + " age is unknown and uses " + weapon + " to fight.";
        }
        else {
            return Integer.toString(idFF7Character) + " " + name + " is " + age + " and uses " + weapon + " to fight.";
        }
    }
}

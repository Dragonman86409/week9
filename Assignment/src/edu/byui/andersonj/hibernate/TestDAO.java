
package edu.byui.andersonj.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/*
 *  Used to get Characters data from my MYSQL database
*/
public class TestDAO {

    SessionFactory factory = null;
    Session session = null;

    private static TestDAO single_instance = null;

    private TestDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static TestDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }

        return single_instance;
    }

    /** Used to get more than one Characters from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<Characters> getCharacterss() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from edu.byui.andersonj.hibernate.Characters";
            List<Characters> cs = (List<Characters>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single Characters from database */
    public Characters getCharacters(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from edu.byui.andersonj.hibernate.Characters where idFF7Character=" + Integer.toString(id);
            Characters c = (Characters)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
